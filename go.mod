module gitlab.com/internal-handbook/internal-handbook.gitlab.io

go 1.19

require (
	github.com/FortAwesome/Font-Awesome v0.0.0-20240108205627-a1232e345536 // indirect
	github.com/google/docsy v0.9.0 // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
	github.com/hairyhenderson/go-codeowners v0.3.1-0.20230710165731-859d0d06aff1 // indirect
	github.com/twbs/bootstrap v5.3.2+incompatible // indirect
	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.3.12 // indirect
)
