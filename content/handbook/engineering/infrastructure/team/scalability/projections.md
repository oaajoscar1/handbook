---
aliases: /handbook/engineering/infrastructure/team/scalability/projections.html
title: "Scalability:Projections Team (deprecated)"
---

## Projections (deprecated team page)

**This team is deprecated in favour of [Scalability:Observability](../observability). Handbook updates to follow.**

This team focuses on forecasting & projection systems that enable development engineering to understand
system growth (planned and unplanned) for their areas of responsibility. Error Budgets and Stage Group Dashboards
are examples of successful projects that have provided development teams information about how their code runs on GitLab.com.

As Dedicated becomes more mature, we will expand our remit to include projection activities for this platform.

We use metrics to gather data to inform our decisions. We contribute to the observability of the system by maintaining
metrics that concern saturation and improving observability tools that we can use to help us understand how the system
responds to load.

