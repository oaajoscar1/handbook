---
title: Duo Chat Group
description: "The Duo Chat group is focused on adding support to GitLab Duo Chat functionality, and how to support other product groups and the wider community in adding functionality."
---

## Vision

The Duo Chat group is focused on adding support to GitLab Duo Chat functionality, and how to support other product groups and the wider community in adding functionality

### 🚀 Team Members

**Engineering Manager & Engineers**

{{< team-by-manager-slug "davidoregan" >}}

**Product, Design & Quality**

{{% stable-counterparts manager-role="Engineering Manager(.*)AI Framework" role="AI Framework" %}}


## 🔗 Other Useful Links

### 📝 Dashboards (internal only)

- [Duo Chat Question Categorization](https://10az.online.tableau.com/#/site/gitlab/views/DuoCategoriesofQuestions/DuoCategory?:iid=1)
- [Chat QA Evaluation](https://gitlab.com/gitlab-org/ai-powered/ai-framework/qa-evaluation)
